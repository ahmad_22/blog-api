## About project
blog management api small task   

## step to start
 
- database name : blog_management_api
- database username : root
- database password : 

- composer install 
- php artisan migrate
- php artisan passport:install
- php artisan db:seed

## login  

- user data
- email : ahmad@gmail.com
- password : Ahmad@1234 

## mvc with laravel basic tools  

- request Form Validation
- resource api 
- trait
- exception
- job
- enum

## note 

- post man link : https://www.getpostman.com/collections/82db1a2b8b2656c3c780
- .env file edit MAIL configurations and the APP_URL
