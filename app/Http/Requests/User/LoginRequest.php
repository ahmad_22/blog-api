<?php

namespace App\Http\Requests\User;

use App\Traits\FormRequestTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class LoginRequest extends FormRequest
{
    use FormRequestTrait;

    public function rules()
    {
        return [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', Password::min(8)->mixedCase()->numbers()]
        ];
    }
}
