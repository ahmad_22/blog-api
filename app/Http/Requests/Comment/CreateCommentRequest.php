<?php

namespace App\Http\Requests\Comment;

use App\Enums\BlogTypeEnum;
use App\Traits\FormRequestTrait;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateCommentRequest extends FormRequest
{
    use FormRequestTrait;

    public function rules()
    {
        return [
            'blog_id' => ['required', 'integer', Rule::exists('blogs', 'id')
                ->whereNull('deleted_at')],
            'comment' => ['required', 'string'],
        ];
    }
}
