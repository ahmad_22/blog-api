<?php

namespace App\Http\Requests\Comment;

use App\Enums\BlogTypeEnum;
use App\Traits\FormRequestTrait;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DeleteCommentRequest extends FormRequest
{
    use FormRequestTrait;

    public function rules()
    {
        return [
            'comment_id' => ['required', 'integer', Rule::exists('comments', 'id')
                ->where('user_id', $this->user()->id)
                ->whereNull('deleted_at')],
        ];
    }
}
