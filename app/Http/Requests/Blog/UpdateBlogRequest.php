<?php

namespace App\Http\Requests\Blog;

use App\Enums\BlogTypeEnum;
use App\Traits\FormRequestTrait;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateBlogRequest extends FormRequest
{
    use FormRequestTrait;

    public function rules()
    {
        return [
            'blog_id' => ['required', 'integer'],
            'title' => ['required', 'string'],
            'description' => ['nullable', 'string', 'max:100'],
            'text_detail' => ['nullable', 'string'],
            'video' => ['nullable', 'file', 'mimetypes:video/mp4'],
        ];
    }
}
