<?php

namespace App\Http\Requests\Blog;

use App\Enums\BlogTypeEnum;
use App\Traits\FormRequestTrait;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DeleteBlogRequest extends FormRequest
{
    use FormRequestTrait;

    public function rules()
    {
        return [
            'blog_id' => ['required', 'integer', Rule::exists('blogs', 'id')
                ->where('user_id', $this->user()->id)
                ->whereNull('deleted_at')],
        ];
    }
}
