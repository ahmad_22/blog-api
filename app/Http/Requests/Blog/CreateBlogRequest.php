<?php

namespace App\Http\Requests\Blog;

use App\Enums\BlogTypeEnum;
use App\Traits\FormRequestTrait;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateBlogRequest extends FormRequest
{
    use FormRequestTrait;

    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'description' => ['nullable', 'string', 'max:100'],
            'text_detail' => ['nullable', Rule::requiredIf(empty($this->video)), 'string'],
            'video' => ['nullable', Rule::requiredIf(empty($this->text_detail)), 'file', 'mimetypes:video/mp4'],
        ];
    }
}
