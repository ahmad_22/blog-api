<?php

namespace App\Http\Resources\BlogDetail;

use Illuminate\Http\Resources\Json\JsonResource;

class BlogDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'object' => 'blog_detail',
            'id' => $this->id,
            'detail' => $this->detail,
        ];
    }
}
