<?php

namespace App\Http\Resources\Blog;

use App\Http\Resources\BlogDetail\BlogDetailResource;
use App\Http\Resources\Comment\CommentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BlogWithRelationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'object' => 'blog',
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'created_date' => $this->created_at,
            'details' => BlogDetailResource::make($this->details),
            'comments' => CommentResource::collection($this->comments),
        ];
    }
}
