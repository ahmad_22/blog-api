<?php

namespace App\Http\Controllers;

use App\Enums\BlogTypeEnum;
use App\Helpers\ResponseHelper;
use App\Http\Requests\Blog\CreateBlogRequest;
use App\Http\Requests\Blog\DeleteBlogRequest;
use App\Http\Requests\Blog\FindBlogRequest;
use App\Http\Requests\Blog\GetBlogRequest;
use App\Http\Requests\Blog\UpdateBlogRequest;
use App\Http\Resources\Blog\BlogResource;
use App\Http\Resources\Blog\BlogWithRelationResource;
use App\Jobs\SendEmailJob;
use App\Models\Blog;
use App\Models\BlogDetail;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    use HelperTrait;
    private $blog, $blogDetail;
    public function __construct(Blog $blog, BlogDetail $blogDetail)
    {
        $this->blog = $blog;
        $this->blogDetail = $blogDetail;
    }

    public function create(CreateBlogRequest $request)
    {
        $dataBlog['title'] = $request->get('title');
        $dataBlog['description'] = $request->get('description', null);
        $dataBlog['user_id'] = $request->user()->id;
        if ($request->file('video')) {
            $detail = $this->uploadVideo($request->file('video'), Str::uuid());
            $dataBlog['type'] = BlogTypeEnum::video;
        } else {
            $detail = $request->get('text_detail');
            $dataBlog['type'] = BlogTypeEnum::text;
        }
        $createdBlog = $this->blog->createData($dataBlog);
        $this->createBlogDetail($detail, $createdBlog->id);
        dispatch(new SendEmailJob());
        return ResponseHelper::create(BlogResource::make($createdBlog));
    }

    public function update(UpdateBlogRequest $request)
    {
        $data = $request->validated();
        $blogId = $data['blog_id'];
        // check if the blog exist and for same user
        $blog = $this->blog->findData([['id', '=', $blogId], ['user_id', '=', $request->user()->id]], ['details']);
        if (empty($blog)) return ResponseHelper::DataNotFound();

        $dataBlog['title'] = $data['title'];
        $dataBlog['description'] = $data['description'] ?? null;

        $type = null;
        $detail = null;
        if ($request->file('video')) {
            $detail = $this->uploadVideo($request->file('video'), Str::uuid());
            $type = BlogTypeEnum::video;
        }
        elseif ($request->filled('text_detail')) {
            $detail = $request->get('text_detail');
            $type = BlogTypeEnum::text;
        }
        $dataBlog['type'] = !empty($type) ? $type : $blog->type;
        $this->blog->updateData(['id' => $blogId], $dataBlog);
        // check if user send detail to refresh detail database
        if (!empty($type)) {
            $dataBlog['type'] = $type;
            // check old type blog is video to delete
            if ($blog->type  == BlogTypeEnum::video)
                $this->deleteVideo($blog->details->detail);
            $this->deleteBlogDetail($blogId);
            $this->createBlogDetail($detail, $blogId);
        }
        return ResponseHelper::update();
    }

    public function delete(DeleteBlogRequest $request)
    {
        $id = $request->validated();
        $this->deleteBlogDetail($id['blog_id']);
        $this->blog->softDeleteData(['id' => $id['blog_id']]);
        return ResponseHelper::delete();
    }

    public function get(GetBlogRequest $request)
    {
        $option = $request->validated();
        $data = $this->blog->getData(filter: ['user_id' => $request->user()->id], isPaginate: true, limit: $option['limit'] ?? null);
        return ResponseHelper::select(BlogResource::collection($data));
    }

    public function find(FindBlogRequest $request)
    {
        $id = $request->validated();
        $data = $this->blog->findData(['id' => $id['blog_id']], ['details', 'comments']);
        return ResponseHelper::select(BlogWithRelationResource::make($data));
    }

    public function createBlogDetail($detail, $blogId)
    {
        $data['detail'] = $detail;
        $data['blog_id'] = $blogId;
        return $this->blogDetail->createData($data);
    }

    public function deleteBlogDetail($blogId)
    {
        return $this->blogDetail->softDeleteData(['blog_id' => $blogId]);
    }
}
