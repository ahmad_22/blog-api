<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Requests\User\LoginRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function login(LoginRequest $request)
    {
        // return Hash::make('Ahmad@1234');
        $user = $request->only('email', 'password');
        $userLogin = $this->user->login($user['email'], $user['password']);
        if (empty($userLogin))
            return ResponseHelper::authorizationFail();

        return ResponseHelper::operationSuccess(UserResource::make($userLogin));
    }
}
