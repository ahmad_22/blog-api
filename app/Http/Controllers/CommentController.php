<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Http\Requests\Comment\CreateCommentRequest;
use App\Http\Requests\Comment\DeleteCommentRequest;
use App\Http\Requests\Comment\UpdateCommentRequest;
use App\Http\Resources\Comment\CommentResource;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    private $comment;
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function create(CreateCommentRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;
        $created = $this->comment->createData($data);
        return ResponseHelper::create(CommentResource::make($created));
    }

    public function update(UpdateCommentRequest $request)
    {
        $data = $request->validated();
        $commentId = $data['comment_id'];
        unset($data['comment_id']);
        $this->comment->updateData(['id' => $commentId], $data);
        return ResponseHelper::update();
    }

    public function delete(DeleteCommentRequest $request)
    {
        $id = $request->validated();
        $this->comment->softDeleteData(['id' => $id['comment_id']]);
        return ResponseHelper::delete();
    }
}
