<?php


namespace App\Traits;

use App\Enums\DefaultValuesEnum;
use Exception;
use App\Exceptions\CreatingModelFailException;
use App\Exceptions\DeletingModelFailException;
use App\Exceptions\InsertingDataFailException;
use App\Exceptions\UpdatingModelFailException;

trait ModelTrait
{
    public function createData($data)
    {
        try {
            $createdModel = $this::create($data);

            if (empty($createdModel))
                throw new CreatingModelFailException('creating data fail');
        } catch (Exception $exception) {
            throw new CreatingModelFailException($exception->getMessage());
        }

        return $createdModel;
    }

    public function insertData($data)
    {
        try {
            $insertedData = $this->insert($data);

            if (empty($insertedData))
                throw new InsertingDataFailException('inserting data fail');
        } catch (Exception $exception) {
            throw new InsertingDataFailException($exception->getMessage());
        }

        return $insertedData;
    }

    public function updateData($filter, $newData)
    {
        try {
            $updatedDataStatus = $this::where($filter)
                ->update($newData);

            if (empty($updatedDataStatus))
                throw new UpdatingModelFailException('updating data fail');
        } catch (Exception $exception) {
            throw new UpdatingModelFailException($exception->getMessage());
        }

        return $updatedDataStatus;
    }

    public function updateOrCreateData($filter, $data)
    {
        try {
            $queryResult = $this->updateOrCreate($filter, $data);

            if (empty($queryResult))
                throw new UpdatingModelFailException('deleting data fail');
        } catch (Exception $exception) {
            throw new UpdatingModelFailException($exception->getMessage());
        }

        return $queryResult;
    }

    public function softDeleteData($filter)
    {
        try {
            $deletingDataStatus = $this::where($filter)
                ->delete();

            if (empty($deletingDataStatus))
                throw new DeletingModelFailException('deleting data fail');
        } catch (Exception $exception) {
            throw new DeletingModelFailException($exception->getMessage());
        }

        return $deletingDataStatus;
    }

    public function forceDeleteData($filter)
    {
        try {
            $deletingDataStatus = $this::where($filter)
                ->forceDelete();

            if (empty($deletingDataStatus))
                throw new DeletingModelFailException('deleting data fail');
        } catch (Exception $exception) {
            throw new DeletingModelFailException($exception->getMessage());
        }

        return $deletingDataStatus;
    }

    public function findData($filter, $relations = array(), $relationsCount = [], $select = ['*'], $orderType = 'DESC', $orderColumn = 'id')
    {
        return $this::where($filter)
            ->when(!blank($relations), function ($model) use ($relations) {
                return $model->with($relations);
            })
            ->select($select)
            ->when(!blank($relationsCount), function ($model) use ($relationsCount) {
                return $model->withCount($relationsCount);
            })
            ->orderBy($orderColumn, $orderType)
            ->first();
    }

    public function getData($filter = array(), $relations = array(), $relationsCount = array(), $select = ['*'], $orderType = 'DESC', $orderColumn = 'id',  $isPaginate = false, $limit = 15)
    {
        $query = $this::where($filter)
            ->when(!blank($relations), function ($model) use ($relations) {
                return $model->with($relations);
            })
            ->select($select)
            ->when(!blank($relationsCount), function ($model) use ($relationsCount) {
                return $model->withCount($relationsCount);
            })
            ->orderBy($orderColumn, $orderType);
        return ($isPaginate) ? $query->paginate($limit) : $query->get();
    }

    public function allData($filter = array(), $orderType = 'DESC', $orderColumn = 'id', $isPaginate = true, $limit = 15)
    {
        $query = $this::when(!blank($filter), function ($query) use ($filter) {
            $query->where($filter);
        })->orderBy($orderColumn, $orderType);

        return ($isPaginate) ? $query->paginate($limit) : $query->get();
    }
}
