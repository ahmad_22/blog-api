<?php


namespace App\Traits;

use App\Models\Store\SocialMedia;
use App\Models\User\Favorite;
use Illuminate\Support\Facades\Storage;

trait HelperTrait
{
    public function uploadVideo($video, $name)
    {
        $fileName = $name . '.' . $video->extension();
        $filePath = 'videos/' . $fileName;
        Storage::disk('public')->put($filePath, file_get_contents($video));

        //  url to access the video in frontend
        $url = env("APP_URL") . $filePath;

        return $url;
    }

    public function deleteVideo($url)
    {
        try {
            return unlink($url);
        } catch (\Throwable $th) {
        }
    }
}
