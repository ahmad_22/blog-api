<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class BlogTypeEnum extends Enum
{
    const text =   'text';
    const video =   'video';
}
