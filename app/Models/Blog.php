<?php

namespace App\Models;

use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use HasFactory, SoftDeletes, ModelTrait;

    protected $table = 'blogs';

    protected $fillable = ['title', 'description', 'type','user_id'];

    protected $hidden = ['deleted_at'];

    public function details()
    {
        return $this->hasOne(BlogDetail::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
