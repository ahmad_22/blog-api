<?php

namespace App\Models;

use App\Traits\ModelTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogDetail extends Model
{
    use HasFactory, SoftDeletes, ModelTrait;

    protected $table = 'blog_details';

    protected $fillable = ['detail', 'blog_id',];

    protected $hidden = ['deleted_at'];
}
