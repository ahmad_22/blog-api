<?php


namespace App\Exceptions;

use Exception;
use App\Helpers\ResponseHelper;

class UpdatingModelFailException extends Exception
{
    public function report()
    {
        return false;
    }

    public function render($request)
    {
        if ($request->expectsJson())
            return ResponseHelper::updatingFail($this->message);

        abort(500);
    }
}
