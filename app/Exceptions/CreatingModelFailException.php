<?php


namespace App\Exceptions;

use Exception;
use App\Helpers\ResponseHelper;

class CreatingModelFailException extends Exception
{
    public function report()
    {
        return false;
    }

    public function render($request)
    {
        if ($request->expectsJson())
            return ResponseHelper::creatingFail($this->message);

        abort(500);
    }
}
