<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\CommentController;
use Illuminate\Support\Facades\Route;

Route::controller(BlogController::class)->middleware('auth:api')->group(function () {
    Route::post('create',  'create');
    Route::post('update',  'update');
    Route::delete('delete/{blog_id}',  'delete');
    Route::get('get', 'get');
    Route::get('find/{blog_id}',  'find');
});

Route::prefix('comment')->controller(CommentController::class)->middleware('auth:api')->group(function () {
    Route::post('create',  'create');
    Route::put('update/{comment_id}',  'update');
    Route::delete('delete/{comment_id}',  'delete');
});
