<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=> 'ahmad',
            'email'=> 'ahmad@gmail.com',
            'password'=> '$2y$10$cGiR9gflub6k.x0RVFvDM.nIckgiq3S94Zjx5L/WVDb2ug/Je8Tz.', //Ahmad@1234
        ]);
    }
}
