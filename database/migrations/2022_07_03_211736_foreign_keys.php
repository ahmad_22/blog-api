<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blogs', function (Blueprint $table) {
            $table->foreignId('user_id')->constrained('users', 'id');
        });
        Schema::table('blog_details', function (Blueprint $table) {
            $table->foreignId('blog_id')->constrained('blogs', 'id');
        });
        Schema::table('comments', function (Blueprint $table) {
            $table->foreignId('blog_id')->constrained('blogs', 'id');
            $table->foreignId('user_id')->constrained('users', 'id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
